package com.zdw.zfw.admin.modular.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zdw.zfw.admin.modular.system.dao.SysUserTokenMapper;
import com.zdw.zfw.admin.modular.system.model.entity.SysUserToken;
import com.zdw.zfw.admin.modular.system.service.ISysUserTokenService;
import com.zdw.zfw.admin.core.util.TokenGenerator;
import com.zdw.zfw.core.page.MybatisPageHelper;
import com.zdw.zfw.core.page.PageRequest;
import com.zdw.zfw.core.page.PageResult;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service
public class SysUserTokenServiceImpl extends ServiceImpl<SysUserTokenMapper, SysUserToken> implements ISysUserTokenService {
	
	// 12小时后过期
	private final static int EXPIRE = 3600 * 12;

	@Override
	public SysUserToken findByUserId(Long userId) {
		return this.baseMapper.findByUserId(userId);
	}

	@Override
	public SysUserToken findByToken(String token) {
		return this.baseMapper.findByToken(token);
	}

	@Override
	public int saveRecord(SysUserToken record) {
		if(record.getId() == null || record.getId() == 0) {
			return this.baseMapper.insertSelective(record);
		}
		return this.baseMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int delete(SysUserToken record) {
		return this.baseMapper.deleteByPrimaryKey(record.getId());
	}

	@Override
	public int delete(List<SysUserToken> records) {
		for(SysUserToken record:records) {
			delete(record);
		}
		return 1;
	}

	@Override
	public SysUserToken findById(Long id) {
		return this.baseMapper.selectByPrimaryKey(id);
	}

	@Override
	public PageResult findPage(PageRequest pageRequest) {
		return MybatisPageHelper.findPage(pageRequest, this.baseMapper);
	}

	@Override
	public SysUserToken createToken(long userId) {
		// 生成一个token
		String token = TokenGenerator.generateToken();
		// 当前时间
		Date now = new Date();
		// 过期时间
		Date expireTime = new Date(now.getTime() + EXPIRE * 1000);
		// 判断是否生成过token
		SysUserToken sysUserToken = findByUserId(userId);
		if(sysUserToken == null){
			sysUserToken = new SysUserToken();
			sysUserToken.setUserId(userId);
			sysUserToken.setToken(token);
			sysUserToken.setLastUpdateTime(now);
			sysUserToken.setExpireTime(expireTime);
			// 保存token，这里选择保存到数据库，也可以放到Redis或Session之类可存储的地方
			saveRecord(sysUserToken);
		} else{
			sysUserToken.setToken(token);
			sysUserToken.setLastUpdateTime(now);
			sysUserToken.setExpireTime(expireTime);
			// 如果token已经生成，则更新token的过期时间
			saveRecord(sysUserToken);
		}
		return sysUserToken;
	}
}
