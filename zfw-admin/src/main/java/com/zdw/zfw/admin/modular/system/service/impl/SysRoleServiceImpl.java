package com.zdw.zfw.admin.modular.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zdw.zfw.admin.core.constants.SysConstants;
import com.zdw.zfw.admin.modular.system.dao.SysMenuMapper;
import com.zdw.zfw.admin.modular.system.dao.SysRoleMapper;
import com.zdw.zfw.admin.modular.system.dao.SysRoleMenuMapper;
import com.zdw.zfw.admin.modular.system.model.entity.SysMenu;
import com.zdw.zfw.admin.modular.system.model.entity.SysRole;
import com.zdw.zfw.admin.modular.system.model.entity.SysRoleMenu;
import com.zdw.zfw.admin.modular.system.service.ISysRoleService;
import com.zdw.zfw.core.page.ColumnFilter;
import com.zdw.zfw.core.page.MybatisPageHelper;
import com.zdw.zfw.core.page.PageRequest;
import com.zdw.zfw.core.page.PageResult;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * SysRoleService的实现类
 *
 * @author zdw
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

	private final SysRoleMenuMapper sysRoleMenuMapper;
	private final SysMenuMapper sysMenuMapper;

	@Override
	public int saveRecord(SysRole record) {
		if(record.getId() == null || record.getId() == 0) {
			return this.baseMapper.insertSelective(record);
		}
		return this.baseMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int delete(SysRole record) {
		return this.baseMapper.deleteByPrimaryKey(record.getId());
	}

	@Override
	public int delete(List<SysRole> records) {
		for(SysRole record:records) {
			delete(record);
		}
		return 1;
	}

	@Override
	public SysRole findById(Long id) {
		return this.baseMapper.selectByPrimaryKey(id);
	}

	@Override
	public PageResult findPage(PageRequest pageRequest) {
		ColumnFilter columnFilter = pageRequest.getColumnFilter("name");
		if(columnFilter != null && columnFilter.getValue() != null) {
			return MybatisPageHelper.findPage(pageRequest, this.baseMapper, "findPageByName", columnFilter.getValue());
		}
		return MybatisPageHelper.findPage(pageRequest, this.baseMapper);
	}

	@Override
	public List<SysRole> findAll() {
		return this.baseMapper.findAll();
	}

	public SysRoleMapper getSysRoleMapper() {
		return this.baseMapper;
	}

	public void setSysRoleMapper(SysRoleMapper sysRoleMapper) {
		this.baseMapper = sysRoleMapper;
	}

	@Override
	public List<SysMenu> findRoleMenus(Long roleId) {
		SysRole sysRole = this.baseMapper.selectByPrimaryKey(roleId);
		if(SysConstants.ADMIN.equalsIgnoreCase(sysRole.getName())) {
			// 如果是超级管理员，返回全部
			return sysMenuMapper.findAll();
		}
		return sysMenuMapper.findRoleMenus(roleId);
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public int saveRoleMenus(List<SysRoleMenu> records) {
		if(records == null || records.isEmpty()) {
			return 1;
		}
		Long roleId = records.get(0).getRoleId(); 
		sysRoleMenuMapper.deleteByRoleId(roleId);
		for(SysRoleMenu record:records) {
			sysRoleMenuMapper.insertSelective(record);
		}
		return 1;
	}

	@Override
	public List<SysRole> findByName(String name) {
		return this.baseMapper.findByName(name);
	}
	
}
