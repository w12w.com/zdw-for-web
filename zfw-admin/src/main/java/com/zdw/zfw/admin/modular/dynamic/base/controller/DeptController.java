package com.zdw.zfw.admin.modular.dynamic.base.controller;

import com.zdw.zfw.admin.modular.dynamic.annotation.SelectList;
import com.zdw.zfw.admin.modular.system.model.entity.SysDept;
import com.zdw.zfw.admin.modular.system.service.ISysDeptService;

@SelectList(path = "dept/list", entity = SysDept.class, service = ISysDeptService.class)
public class DeptController {
}
