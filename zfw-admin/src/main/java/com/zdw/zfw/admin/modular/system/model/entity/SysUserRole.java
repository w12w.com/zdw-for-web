package com.zdw.zfw.admin.modular.system.model.entity;

import com.zdw.zfw.core.base.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 用户角色实体
 * @author zdw
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
public final class SysUserRole extends BaseModel<SysUserRole> {

    private Long userId;

    private Long roleId;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

}