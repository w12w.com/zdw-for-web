package com.zdw.zfw.admin.modular.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zdw.zfw.admin.modular.system.model.entity.SysDict;
import com.zdw.zfw.core.base.CurdService;

import java.util.List;

/**
 * 字典管理
 * @author zdw
 * @date Oct 29, 2018
 */
public interface ISysDictService extends IService<SysDict>, CurdService<SysDict> {

	/**
	 * 根据名称查询
	 * @param lable
	 * @return
	 */
	List<SysDict> findByLabel(String lable);
}
