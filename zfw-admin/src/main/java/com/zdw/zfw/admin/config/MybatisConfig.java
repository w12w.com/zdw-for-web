package com.zdw.zfw.admin.config;

import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

/**
 * Mybatis配置
 * @author zdw
 * @date Oct 29, 2018
 */
@Configuration
@MapperScan(basePackages = {"**.dao"})	// 扫描DAO
@PropertySource("classpath:/default-config.properties")
public class MybatisConfig {

    /**
     * 设置 dev test local 环境开启执行分析器
     * @return 执行分析器
     */
    @Bean
    @Profile({"local", "dev", "test"})
    public PerformanceInterceptor performanceInterceptor() {
        return new PerformanceInterceptor();
    }
}