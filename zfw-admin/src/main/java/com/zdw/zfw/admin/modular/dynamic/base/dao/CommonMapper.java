package com.zdw.zfw.admin.modular.dynamic.base.dao;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CommonMapper {

    List<Map<String, Object>> selectList(@Param(value = "sql") String sql);
}
