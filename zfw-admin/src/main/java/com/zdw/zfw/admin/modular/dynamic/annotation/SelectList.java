package com.zdw.zfw.admin.modular.dynamic.annotation;

import com.baomidou.mybatisplus.extension.service.IService;

import java.lang.annotation.*;

/**
 * @author zhangdewen
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SelectList {
    /**
     * @return 映射路径
     */
    String path();

    /**
     * @return 实体
     */
    Class<?> entity();

    /**
     * @return service类
     */
    Class<? extends IService<?>> service();
}
