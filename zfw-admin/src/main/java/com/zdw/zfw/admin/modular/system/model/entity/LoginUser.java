package com.zdw.zfw.admin.modular.system.model.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 登录用户的信息
 *
 * @author zdw
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
public final class LoginUser {

    /**
     * 用户id
     */
    private Long id;

    /**
     *  用户名
     */
    private String name;

    /**
     * 用户邮箱
     */
    private String email;

    /**
     * 用户电话
     */
    private String mobile;

    /**
     * 用户状态
     */
    private Byte status;

    /**
     * 部门
     */
    private Long deptId;

    /**
     * 部门名
     */
    private String deptName;

    /**
     * 删除标记
     */
    private Byte delFlag;

    /**
     * 角色名
     */
    private String roleNames;

    /**
     * 简介
     */
    private String introduction;

    /**
     * 拥有角色
     */
    private List<SysUserRole> userRoles = new ArrayList<>();

    /**
     * 拥有权限
     */
    private Set<String> permissions;
}
