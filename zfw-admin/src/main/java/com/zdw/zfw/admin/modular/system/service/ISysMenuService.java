package com.zdw.zfw.admin.modular.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zdw.zfw.admin.modular.system.model.entity.SysMenu;
import com.zdw.zfw.admin.modular.system.model.vo.SysMenuVO;
import com.zdw.zfw.core.base.CurdService;

import java.util.List;

/**
 * 菜单管理
 * @author zdw
 * @date Oct 29, 2018
 */
public interface ISysMenuService extends IService<SysMenu>, CurdService<SysMenu> {

	/**
	 * 查询菜单树,用户ID和用户名为空则查询全部
	 * @param menuType 获取菜单类型，0：获取所有菜单，包含按钮，1：获取所有菜单，不包含按钮
	 * @param userName 用户名
	 * @return
	 */
	List<SysMenuVO> findTree(String userName, int menuType);

	/**
	 * 根据用户名查找菜单列表
	 * @param userName
	 * @return
	 */
	List<SysMenu> findByUser(String userName);
}
