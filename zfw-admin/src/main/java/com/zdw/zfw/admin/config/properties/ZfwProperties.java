package com.zdw.zfw.admin.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 项目的配置
 *
 * @author zdw
 */
@ConfigurationProperties(prefix = ZfwProperties.ZFW_PREFIX)
public class ZfwProperties {
     static final String ZFW_PREFIX = "zfw";

    private String avatarAddress;

    public String getAvatarAddress() {
        return avatarAddress;
    }

    public void setAvatarAddress(String avatarAddress) {
        this.avatarAddress = avatarAddress;
    }
}
