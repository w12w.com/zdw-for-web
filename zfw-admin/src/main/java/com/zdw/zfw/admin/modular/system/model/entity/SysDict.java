package com.zdw.zfw.admin.modular.system.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zdw.zfw.core.base.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 字典实体
 * @author zdw
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
@TableName("sys_dict")
public final class SysDict extends BaseModel<SysDict> {

    private String value;

    private String label;

    private String type;

    private String description;

    private Long sort;

    private String remarks;

    @TableField(value = "del_flag")
    private Byte delFlag;
}