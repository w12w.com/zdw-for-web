package com.zdw.zfw.admin.modular.system.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zdw.zfw.core.base.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 菜单实体
 * @author zdw
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
@TableName("sys_menu")
public final class SysMenu extends BaseModel<SysMenu> {

    @TableField(value = "parent_id")
    private Long parentId;

    private String name;

    private String url;

    private String perms;

    private Integer type;

    private String icon;

    @TableField(value = "hide_in_menu")
    private boolean hideInMenu;

    @TableField(value = "order_num")
    private Integer orderNum;

    @TableField(value = "del_flag")
    private Byte delFlag;

    /**
     * 非数据库字段
     */
    @TableField(exist = false)
    private String parentName;

    /**
     * 非数据库字段
     */
    @TableField(exist = false)
    private Integer level;

    /**
     * 非数据库字段
     */
    @TableField(exist = false)
    private List<SysMenu> children;
}