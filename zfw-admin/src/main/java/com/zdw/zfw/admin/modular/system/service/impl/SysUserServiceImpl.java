package com.zdw.zfw.admin.modular.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zdw.zfw.admin.modular.system.dao.SysRoleMapper;
import com.zdw.zfw.admin.modular.system.dao.SysUserRoleMapper;
import com.zdw.zfw.admin.modular.system.model.entity.SysMenu;
import com.zdw.zfw.admin.modular.system.model.entity.SysRole;
import com.zdw.zfw.admin.modular.system.model.entity.SysUserRole;
import com.zdw.zfw.admin.modular.system.dao.SysUserMapper;
import com.zdw.zfw.admin.modular.system.model.entity.SysUser;
import com.zdw.zfw.admin.modular.system.service.ISysUserService;
import com.zdw.zfw.admin.modular.system.service.ISysMenuService;
import com.zdw.zfw.core.page.ColumnFilter;
import com.zdw.zfw.core.page.MybatisPageHelper;
import com.zdw.zfw.core.page.PageRequest;
import com.zdw.zfw.core.page.PageResult;
import lombok.RequiredArgsConstructor;
import lombok.experimental.ExtensionMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @author zdw
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@ExtensionMethod(ServiceImpl.class)
//@lombok.extern.mybatisplus.ServiceImpl
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

	private final ISysMenuService sysMenuService;
	private final SysUserRoleMapper sysUserRoleMapper;
	private final SysRoleMapper sysRoleMapper;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public int saveRecord(SysUser record) {
		int updateRows;
		if(record.getId() == null || record.getId() == 0) {
			// 新增用户
			updateRows = this.baseMapper.insertSelective(record);
		} else {
			// 更新用户信息
			updateRows = this.baseMapper.updateByPrimaryKeySelective(record);
		}
		// 更新用户角色
		List<SysUserRole> userRoles = record.getUserRoles();
		if (userRoles!= null && userRoles.size() > 0) {
			sysUserRoleMapper.deleteByUserId(record.getId());
			for(SysUserRole sysUserRole : userRoles) {
				sysUserRoleMapper.insertSelective(sysUserRole);
			}
		}
		return updateRows;
	}

	@Override
	public int delete(SysUser record) {
		return this.baseMapper.deleteByPrimaryKey(record.getId());
	}

	@Override
	public int delete(List<SysUser> records) {
		for(SysUser record:records) {
			delete(record);
		}
		return 1;
	}

	@Override
	public SysUser findById(Long id) {
		return this.baseMapper.selectByPrimaryKey(id);
	}
	
	@Override
	public SysUser findByName(String name) {
		return this.baseMapper.findByName(name);
	}
	
	@Override
	public PageResult findPage(PageRequest pageRequest) {
		PageResult pageResult = null;
		String name = getColumnFilterValue(pageRequest, "name");
		String email = getColumnFilterValue(pageRequest, "email");
		if(name != null) {
			if(email != null) {
				pageResult = MybatisPageHelper.findPage(pageRequest, this.baseMapper, "findPageByNameAndEmail", name, email);
			} else {
				pageResult = MybatisPageHelper.findPage(pageRequest, this.baseMapper, "findPageByName", name);
			}
		} else {
			pageResult = MybatisPageHelper.findPage(pageRequest, this.baseMapper);
		}
		// 加载用户角色信息
		findUserRoles(pageResult);
		return pageResult;
	}

	/**
	 * 获取过滤字段的值
	 * @param filterName
	 * @return
	 */
	public String getColumnFilterValue(PageRequest pageRequest, String filterName) {
		String value = null;
		ColumnFilter columnFilter = pageRequest.getColumnFilter(filterName);
		if(columnFilter != null) {
			value = columnFilter.getValue();
		}
		return value;
	}
	
	/**
	 * 加载用户角色
	 * @param pageResult
	 */
	private void findUserRoles(PageResult pageResult) {
		List<?> content = pageResult.getContent();
		for(Object object:content) {
			SysUser sysUser = (SysUser) object;
			List<SysUserRole> userRoles = findUserRoles(sysUser.getId());
			sysUser.setUserRoles(userRoles);
			sysUser.setRoleNames(getRoleNames(userRoles));
		}
	}

	private String getRoleNames(List<SysUserRole> userRoles) {
		StringBuilder sb = new StringBuilder();
		for(Iterator<SysUserRole> iter=userRoles.iterator(); iter.hasNext();) {
			SysUserRole userRole = iter.next();
			SysRole sysRole = sysRoleMapper.selectByPrimaryKey(userRole.getRoleId());
			if(sysRole == null) {
				continue ;
			}
			sb.append(sysRole.getRemark());
			if(iter.hasNext()) {
				sb.append(", ");
			}
		}
		return sb.toString();
	}

	@Override
	public Set<String> findPermissions(String userName) {	
		Set<String> perms = new HashSet<>();
		List<SysMenu> sysMenus = sysMenuService.findByUser(userName);
		for(SysMenu sysMenu:sysMenus) {
			String perm = sysMenu.getPerms();
			if (perm != null){
				perms.add(perm);
			}
		}
		return perms;
	}

	@Override
	public List<SysUserRole> findUserRoles(Long userId) {
		return sysUserRoleMapper.findUserRoles(userId);
	}
}
