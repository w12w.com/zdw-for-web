package com.zdw.zfw.admin.modular.system.controller;

import com.zdw.zfw.admin.modular.system.model.entity.SysMenu;
import com.zdw.zfw.admin.modular.system.service.ISysMenuService;
import com.zdw.zfw.core.http.HttpResult;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 菜单控制器
 * @author zdw
 * @date Oct 29, 2018
 */
@RestController
@RequestMapping("menu")
@RequiresAuthentication
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SysMenuController {

	/**
	 * 菜单 service
	 */
	private final ISysMenuService sysMenuService;
	
	@PostMapping(value="/save")
	public HttpResult save(@RequestBody SysMenu record) {
		return HttpResult.success(sysMenuService.saveRecord(record));
	}

	@PostMapping(value="/delete")
	public HttpResult delete(@RequestBody List<SysMenu> records) {
		return HttpResult.success(sysMenuService.delete(records));
	}

	@GetMapping(value="/findNavTree")
	public HttpResult findNavTree(@RequestParam String userName) {
		return HttpResult.success(sysMenuService.findTree(userName, 1));
	}
	
	@GetMapping(value="/findMenuTree")
	public HttpResult findMenuTree() {
		return HttpResult.success(sysMenuService.findTree(null, 0));
	}
}
