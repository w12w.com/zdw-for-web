package com.zdw.zfw.admin.modular.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zdw.zfw.admin.modular.system.dao.SysLogMapper;
import com.zdw.zfw.admin.modular.system.model.entity.SysLog;
import com.zdw.zfw.admin.modular.system.service.ISysLogService;
import com.zdw.zfw.core.page.ColumnFilter;
import com.zdw.zfw.core.page.MybatisPageHelper;
import com.zdw.zfw.core.page.PageRequest;
import com.zdw.zfw.core.page.PageResult;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zdw
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements ISysLogService {

	@Override
	public int saveRecord(SysLog record) {
		if(record.getId() == null || record.getId() == 0) {
			return this.baseMapper.insertSelective(record);
		}
		return this.baseMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int delete(SysLog record) {
		return this.baseMapper.deleteByPrimaryKey(record.getId());
	}

	@Override
	public int delete(List<SysLog> records) {
		for(SysLog record:records) {
			delete(record);
		}
		return 1;
	}

	@Override
	public SysLog findById(Long id) {
		return this.baseMapper.selectByPrimaryKey(id);
	}

	@Override
	public PageResult findPage(PageRequest pageRequest) {
		ColumnFilter columnFilter = pageRequest.getColumnFilter("userName");
		if(columnFilter != null) {
			return MybatisPageHelper.findPage(pageRequest, this.baseMapper, "findPageByUserName", columnFilter.getValue());
		}
		return MybatisPageHelper.findPage(pageRequest, this.baseMapper);
	}
}
