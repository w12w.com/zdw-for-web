package com.zdw.zfw.admin.modular.system.controller;

import com.zdw.zfw.admin.modular.system.model.entity.SysDept;
import com.zdw.zfw.admin.modular.system.service.ISysDeptService;
import com.zdw.zfw.core.http.HttpResult;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 机构控制器
 * @author zdw
 * @date Oct 29, 2018
 */
@RestController
@RequestMapping("dept")
@RequiresAuthentication
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SysDeptController {

	/**
	 * 机构 service
	 */
	private final ISysDeptService sysDeptService;
	
	@PostMapping(value="/save")
	public HttpResult save(@RequestBody SysDept record) {
		return HttpResult.success(sysDeptService.saveRecord(record));
	}

	@PostMapping(value="/delete")
	public HttpResult delete(@RequestBody List<SysDept> records) {
		return HttpResult.success(sysDeptService.delete(records));
	}

	@GetMapping(value="/findTree")
	public HttpResult findTree() {
		return HttpResult.success(sysDeptService.findTree());
	}

}
