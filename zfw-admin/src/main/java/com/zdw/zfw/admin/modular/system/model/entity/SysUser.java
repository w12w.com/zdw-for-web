package com.zdw.zfw.admin.modular.system.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zdw.zfw.core.base.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户实体
 *
 * @author zdw
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
@TableName("sys_user")
public class SysUser extends BaseModel<SysUser> {
    /**
     * 名字
     */
    private String name;

    /**
     * 密码
     */
    private String password;

    /**
     * 盐
     */
    private String salt;

    /**
     * 邮件
     */
    private String email;

    /**
     * 电话
     */
    private String mobile;

    /**
     * 状态
     */
    private Byte status;

    /**
     * 简介
     */
    private String introduction;

    @TableField("dept_id")
    private Long deptId;

    @TableField("dept_name")
    private String deptName;

    @TableField("del_flag")
    private Byte delFlag;

    @TableField(exist = false)
    private String roleNames;

    @TableField(exist = false)
    private List<SysUserRole> userRoles = new ArrayList<>();
}