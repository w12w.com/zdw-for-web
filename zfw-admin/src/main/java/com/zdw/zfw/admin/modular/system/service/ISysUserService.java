package com.zdw.zfw.admin.modular.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zdw.zfw.admin.modular.system.model.entity.SysUser;
import com.zdw.zfw.admin.modular.system.model.entity.SysUserRole;
import com.zdw.zfw.core.base.CurdService;

import java.util.List;
import java.util.Set;

/**
 * 用户管理
 * @author zdw
 * @date Oct 29, 2018
 */
public interface ISysUserService extends IService<SysUser>, CurdService<SysUser> {

	/**
	 * 根据用户名获取
	 *
	 * @param username 用户名
	 * @return 用户实体
	 */
	SysUser findByName(String username);

	/**
	 * 查找用户的菜单权限标识集合
	 *
	 * @param userName 用户账号
	 * @return 用户的菜单权限标识集合
	 */
	Set<String> findPermissions(String userName);

	/**
	 * 查找用户的角色集合
	 *
	 * @param userId 用户id
	 * @return 用户的角色集合
	 */
	List<SysUserRole> findUserRoles(Long userId);

}
