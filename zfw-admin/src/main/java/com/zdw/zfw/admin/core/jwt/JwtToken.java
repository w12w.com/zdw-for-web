package com.zdw.zfw.admin.core.jwt;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * @author zdw
 */
public class JwtToken implements AuthenticationToken {

    // 密钥
    private final String token;

    public JwtToken(String token) {
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}
