package com.zdw.zfw.admin.modular.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zdw.zfw.admin.modular.system.model.entity.SysUserToken;
import org.apache.ibatis.annotations.Param;

public interface SysUserTokenMapper extends BaseMapper<SysUserToken> {
    int deleteByPrimaryKey(Long id);

    int insert(SysUserToken record);

    int insertSelective(SysUserToken record);

    SysUserToken selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysUserToken record);

    int updateByPrimaryKey(SysUserToken record);
    
    SysUserToken findByUserId(@Param(value = "userId") Long userId);

    SysUserToken findByToken(@Param(value = "token") String token);
}