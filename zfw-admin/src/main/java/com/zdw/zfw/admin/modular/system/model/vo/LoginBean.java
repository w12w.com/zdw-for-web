package com.zdw.zfw.admin.modular.system.model.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 登录接口封装对象
 * @author zdw
 * @date Oct 29, 2018
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
public class LoginBean {

	/**
	 * 账户
	 */
	private String account;

	/**
	 * 密码
	 */
	private String password;

	/**
	 * 校验码
	 */
	private String captcha;
}
