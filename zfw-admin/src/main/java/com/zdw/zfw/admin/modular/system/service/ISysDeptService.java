package com.zdw.zfw.admin.modular.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zdw.zfw.admin.modular.system.model.entity.SysDept;
import com.zdw.zfw.core.base.CurdService;

import java.util.List;

/**
 * 机构管理
 * @author zdw
 * @date Oct 29, 2018
 */
public interface ISysDeptService extends CurdService<SysDept>, IService<SysDept> {

	/**
	 * 查询机构树
	 * @return
	 */
	List<SysDept> findTree();
}
