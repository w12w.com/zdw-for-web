package com.zdw.zfw.admin.modular.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zdw.zfw.admin.modular.system.model.entity.SysUserRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
    /**
     * 根据主键删除
     *
     * @param id 主键
     * @return 删除条数
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 插入一条记录
     *
     * @param record 一条记录
     * @return 插入条数
     */
    @Override
    int insert(SysUserRole record);

    int insertSelective(SysUserRole record);

    SysUserRole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysUserRole record);

    int updateByPrimaryKey(SysUserRole record);

	List<SysUserRole> findUserRoles(@Param(value = "userId") Long userId);

	int deleteByUserId(@Param(value = "userId") Long userId);
}