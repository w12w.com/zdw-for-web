package com.zdw.zfw.admin.modular.dynamic.collector;

import com.baomidou.mybatisplus.annotation.TableName;
import com.google.common.collect.Lists;
import com.zdw.zfw.admin.modular.dynamic.annotation.SelectList;
import com.zdw.zfw.admin.modular.dynamic.base.pojo.ProxyMetaData;
import com.zdw.zfw.core.common.utils.SpringContextHolder;
import org.reflections.Reflections;

import java.util.List;
import java.util.Set;

/**
 * @author zdw
 */
public class CrudCollector {
    private static final List<ProxyMetaData> CLASS_CONTAINER = Lists.newArrayListWithCapacity(16);

    public void collect(){
        Reflections reflections = new Reflections("com.zdw.zfw.admin.modular.dynamic");
        Set<Class<?>> typesAnnotatedWith = reflections.getTypesAnnotatedWith(SelectList.class);

        for (Class<?> aClass : typesAnnotatedWith) {
            ProxyMetaData proxyMetaData = new ProxyMetaData();
            SelectList annotation = aClass.getAnnotation(SelectList.class);
            proxyMetaData.setPath(annotation.path());
            proxyMetaData.setService(SpringContextHolder.getBean(annotation.service()));
            proxyMetaData.setTableName(annotation.entity().getAnnotation(TableName.class).value());
            CLASS_CONTAINER.add(proxyMetaData);
        }
    }

    public static List<ProxyMetaData> getContainer(){
        return CLASS_CONTAINER;
    }

}
