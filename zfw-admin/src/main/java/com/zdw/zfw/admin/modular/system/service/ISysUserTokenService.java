package com.zdw.zfw.admin.modular.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zdw.zfw.admin.modular.system.model.entity.SysUserToken;
import com.zdw.zfw.core.base.CurdService;

/**
 * 用户Token管理
 * @author zdw
 * @date Aug 21, 2018
 */
public interface ISysUserTokenService extends IService<SysUserToken>, CurdService<SysUserToken> {

	/**
	 * 根据用户id查找
	 * @param userId
	 * @return
	 */
	SysUserToken findByUserId(Long userId);

	/**
	 * 根据token查找
	 * @param token
	 * @return
	 */
	SysUserToken findByToken(String token);
	
	/**
	 * 生成token
	 * @param userId
	 * @return
	 */
	SysUserToken createToken(long userId);

}
