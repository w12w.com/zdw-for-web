package com.zdw.zfw.admin.aspect;

/**
 * DAO切面，插入创建人，创建时间，修改人，修改时间
 *
 * @author zdw
 * @date Oct 29, 2018
 */
//@Aspect
//@Component
public class DaoAspect {

//	private static final String CREATE_BY = "createBy";
//	private static final String CREATE_TIME = "createTime";
//	private static final String LASTUPDATE_BY = "lastUpdateBy";
//	private static final String LASTUPDATE_TIME = "lastUpdateTime";
//
//	@Pointcut("execution(* com.zdw.zfw..*.dao.*.update*(..))")
//	public void daoUpdate() {
//	}
//
//	@Pointcut("execution(* com.zdw.zfw..*dao.*.insert*(..))")
//	public void daoCreate() {
//	}
//
//	@Around("daoUpdate()")
//	public Object doAroundUpdate(ProceedingJoinPoint pjp) throws Throwable {
//		LoginUser user = getUserName();
//		if (user != null) {
//			Object[] objects = pjp.getArgs();
//			if (objects != null && objects.length > 0) {
//				for (Object arg : objects) {
//					BeanUtils.setProperty(arg, LASTUPDATE_BY, user.getName());
//					BeanUtils.setProperty(arg, LASTUPDATE_TIME, new Date());
//				}
//			}
//		}
//		return pjp.proceed();
//	}
//
//	@Around("daoCreate()")
//	public Object doAroundCreate(ProceedingJoinPoint pjp) throws Throwable {
//		LoginUser user = getUserName();
//		Object[] objects = pjp.getArgs();
//		if (objects != null && objects.length > 0) {
//			for (Object arg : objects) {
//				if (user != null) {
//					if (StringUtils.isBlank(BeanUtils.getProperty(arg, CREATE_BY))) {
//						BeanUtils.setProperty(arg, CREATE_BY, user.getName());
//					}
//					if (StringUtils.isBlank(BeanUtils.getProperty(arg, CREATE_TIME))) {
//						BeanUtils.setProperty(arg, CREATE_TIME, new Date());
//					}
//				}
//			}
//		}
//		return pjp.proceed();
//	}
//
//	private LoginUser getUserName() {
//		return ShiroUtils.getUser();
//	}
}
