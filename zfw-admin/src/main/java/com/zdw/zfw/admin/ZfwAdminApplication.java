package com.zdw.zfw.admin;

import com.zdw.zfw.admin.config.properties.JwtProperties;
import com.zdw.zfw.admin.config.properties.ZfwProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * 启动器
 * @author zdw
 * @date 2019/2/13
 */
@EnableAspectJAutoProxy
@EnableConfigurationProperties({ZfwProperties.class, JwtProperties.class})
@SpringBootApplication(scanBasePackages={"com.zdw.zfw"})
public class ZfwAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZfwAdminApplication.class, args);
	}
}
