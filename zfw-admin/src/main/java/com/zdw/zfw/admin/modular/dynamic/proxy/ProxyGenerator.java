package com.zdw.zfw.admin.modular.dynamic.proxy;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zdw.zfw.admin.modular.dynamic.base.controller.BaseController;
import com.zdw.zfw.admin.modular.dynamic.base.pojo.ProxyMetaData;
import com.zdw.zfw.admin.modular.dynamic.collector.CrudCollector;
import com.zdw.zfw.core.http.HttpResult;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;


/**
 * 代理生成器
 */
public class ProxyGenerator implements MethodInterceptor {

    public Object getInstance(Class<BaseController> claxx) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(claxx);
        // 回调方法
        enhancer.setCallback(this);
        // 创建代理对象
        return enhancer.create();
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) {
        HttpServletRequest request = ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getRequest();
        String path = request.getRequestURI();
        List list = null;
        List<ProxyMetaData> container = CrudCollector.getContainer();
        for (ProxyMetaData proxyMetaData : container) {
            if (path.equals(String.format("/%s", proxyMetaData.getPath()))) {
                IService service = proxyMetaData.getService();
                QueryWrapper queryWrapper = new QueryWrapper();
                queryWrapper.allEq(((Map) objects[0]));
                list = service.listMaps(queryWrapper);
            }
        }
        return HttpResult.success(list);

//        CommonMapper mapper = SpringContextHolder.getBean(CommonMapper.class);
//        TablesCrudProperties table = SpringContextHolder.getBean("tablesCrudProperties");
//        List<Map<String, String>> tableList = table.getList();
//        String tableName = "";
//        for (Map<String, String> stringStringMap : tableList) {
//            if (path.equals(String.format("/%s", stringStringMap.get("path")))) {
//                tableName = stringStringMap.get("name");
//            }
//        }
//        String sqlBody = String.format("select * from %s", tableName);
//        if (objects[0] != null) {
//            Map<String, Object> conditon = (Map<String, Object>) objects[0];
//            if (!conditon.isEmpty()) {
//                String cond = " where ";
//                for (Map.Entry<String, Object> e : conditon.entrySet()) {
//                    if (!" where ".equals(cond)) {
//                        cond+=" AND ";
//
//                    }
//                    cond+=e.getKey();
//                    cond+="=";
//                    cond+=e.getValue();
//                }
//                sqlBody+=cond;
//            }
//        }
//
//
//        List<Map<String, Object>> result = mapper.selectList(sqlBody);
//
//        return HttpResult.ok(result);
    }
}

