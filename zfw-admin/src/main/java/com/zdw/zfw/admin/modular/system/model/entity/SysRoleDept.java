package com.zdw.zfw.admin.modular.system.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zdw.zfw.core.base.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 角色部门实体
 * @author zdw
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
@TableName("sys_role_dept")
public final class SysRoleDept extends BaseModel<SysRoleDept> {

	@TableField(value = "role_id")
    private Long roleId;

	@TableField(value = "dept_id")
    private Long deptId;
}