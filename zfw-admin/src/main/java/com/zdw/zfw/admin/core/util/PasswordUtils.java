package com.zdw.zfw.admin.core.util;

import java.util.UUID;

import org.apache.shiro.crypto.hash.Sha256Hash;

/**
 * 密码工具类
 *
 * @author zdw
 * @date Sep 1, 2018
 */
public final class PasswordUtils {

	private PasswordUtils() {
	}

	/**
	 * 匹配密码
	 * @param password 密码
	 * @param salt 盐
	 * @param encryptePassword 加密后的密码
	 * @return 是否匹配
	 */
	public static boolean match(String password, String salt, String encryptePassword) {
		return password != null && encrypte(password, salt).equals(encryptePassword);
	}
	
	/**
	 * 明文密码加密
	 *
	 * @param password 密码
	 * @param salt 盐
	 * @return 加密
	 */
	public static String encrypte(String password, String salt) {
		return new Sha256Hash(password, salt).toHex();
	}

	/**
	 * 获取加密盐
	 *
	 * @return 盐
	 */
	public static String getSalt() {
		return UUID.randomUUID().toString().replaceAll("-", "").substring(0, 20);
	}
}
