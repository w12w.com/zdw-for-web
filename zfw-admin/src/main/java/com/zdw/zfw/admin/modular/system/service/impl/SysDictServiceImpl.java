package com.zdw.zfw.admin.modular.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zdw.zfw.admin.modular.system.dao.SysDictMapper;
import com.zdw.zfw.admin.modular.system.model.entity.SysDict;
import com.zdw.zfw.admin.modular.system.service.ISysDictService;
import com.zdw.zfw.core.page.ColumnFilter;
import com.zdw.zfw.core.page.MybatisPageHelper;
import com.zdw.zfw.core.page.PageRequest;
import com.zdw.zfw.core.page.PageResult;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 字典Service Impl
 *
 * @author zdw
 */
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements ISysDictService {

	@Override
	public int saveRecord(SysDict record) {
		if(record.getId() == null || record.getId() == 0) {
			return this.baseMapper.insertSelective(record);
		}
		return this.baseMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int delete(SysDict record) {
		return this.baseMapper.deleteByPrimaryKey(record.getId());
	}

	@Override
	public int delete(List<SysDict> records) {
		for(SysDict record:records) {
			delete(record);
		}
		return 1;
	}

	@Override
	public SysDict findById(Long id) {
		return this.baseMapper.selectByPrimaryKey(id);
	}

	@Override
	public PageResult findPage(PageRequest pageRequest) {
		ColumnFilter columnFilter = pageRequest.getColumnFilter("label");
		if(columnFilter != null) {
			return MybatisPageHelper.findPage(pageRequest, this.baseMapper, "findPageByLabel", columnFilter.getValue());
		}
		return MybatisPageHelper.findPage(pageRequest, this.baseMapper);
	}

	@Override
	public List<SysDict> findByLabel(String lable) {
		return this.baseMapper.findByLable(lable);
	}

}
