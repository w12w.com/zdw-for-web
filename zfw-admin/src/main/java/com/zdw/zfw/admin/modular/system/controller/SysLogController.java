package com.zdw.zfw.admin.modular.system.controller;

import com.zdw.zfw.admin.modular.system.service.ISysLogService;
import com.zdw.zfw.core.http.HttpResult;
import com.zdw.zfw.core.page.PageRequest;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 日志控制器
 * @author zdw
 * @date Oct 29, 2018
 */
@RestController
@RequestMapping("log")
@RequiresAuthentication
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SysLogController {

	/**
	 * 日志 service
	 */
	private final ISysLogService sysLogService;

	@PostMapping(value="/findPage")
	public HttpResult findPage(@RequestBody PageRequest pageRequest) {
		return HttpResult.success(sysLogService.findPage(pageRequest));
	}
}
