package com.zdw.zfw.admin.aspect;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.zdw.zfw.admin.core.util.ShiroUtils;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Objects;

/**
 * 公共字段自动注入主要针对（CREATE_BY CREATE_TIME LAST_UPDATE_BY LAST_UPDATE_TIME）
 *
 * @author zdw
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    private static final String CREATE_BY = "createBy";
    private static final String CREATE_TIME = "createTime";
    private static final String LAST_UPDATE_BY = "lastUpdateBy";
    private static final String LAST_UPDATE_TIME = "lastUpdateTime";



    @Override
    public void insertFill(MetaObject metaObject) {
//        String userName = getUserName();
//        this.setFieldValByName(CREATE_BY, userName, metaObject);
//        this.setFieldValByName(CREATE_TIME, new Date(), metaObject);
    }


    @Override
    public void updateFill(MetaObject metaObject) {
//        String userName = getUserName();
//        this.setFieldValByName(LAST_UPDATE_BY, userName, metaObject);
//        this.setFieldValByName(LAST_UPDATE_TIME, new Date(), metaObject);
    }

    /**
     * 获取当前登录用户名
     *
     * @return 登录用户名
     */
    private String getUserName() {
        return Objects.requireNonNull(ShiroUtils.getUser()).getName();
    }
}
