package com.zdw.zfw.admin.modular.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zdw.zfw.admin.modular.system.model.entity.SysRoleDept;

public interface SysRoleDeptMapper extends BaseMapper<SysRoleDept> {
    int deleteByPrimaryKey(Long id);

    int insert(SysRoleDept record);

    int insertSelective(SysRoleDept record);

    SysRoleDept selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysRoleDept record);

    int updateByPrimaryKey(SysRoleDept record);
}