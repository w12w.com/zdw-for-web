package com.zdw.zfw.admin.modular.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zdw.zfw.admin.modular.system.model.entity.SysMenu;
import com.zdw.zfw.admin.modular.system.model.entity.SysRole;
import com.zdw.zfw.admin.modular.system.model.entity.SysRoleMenu;
import com.zdw.zfw.core.base.CurdService;

import java.util.List;

/**
 * 角色管理
 * @author zdw
 * @date Oct 29, 2018
 */
public interface ISysRoleService extends IService<SysRole>, CurdService<SysRole> {

	/**
	 * 查询全部
	 * @return
	 */
	List<SysRole> findAll();

	/**
	 * 查询角色菜单集合
	 * @return
	 */
	List<SysMenu> findRoleMenus(Long roleId);

	/**
	 * 保存角色菜单
	 * @param records
	 * @return
	 */
	int saveRoleMenus(List<SysRoleMenu> records);

	/**
	 * 根据名称查询
	 * @param name
	 * @return
	 */
	List<SysRole> findByName(String name);

}
