package com.zdw.zfw.admin.modular.system.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zdw.zfw.core.base.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * token实体
 * @author zdw
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
@TableName("sys_user_token")
public final class SysUserToken extends BaseModel<SysUserToken> {

    @TableField("user_id")
    private Long userId;
    
    private String token;

    @TableField("expire_time")
    private Date expireTime;
}