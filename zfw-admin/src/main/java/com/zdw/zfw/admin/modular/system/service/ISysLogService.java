package com.zdw.zfw.admin.modular.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zdw.zfw.admin.modular.system.model.entity.SysLog;
import com.zdw.zfw.core.base.CurdService;

/**
 * 日志管理
 * @author zdw
 * @date Oct 29, 2018
 */
public interface ISysLogService extends IService<SysLog>, CurdService<SysLog> {

}
