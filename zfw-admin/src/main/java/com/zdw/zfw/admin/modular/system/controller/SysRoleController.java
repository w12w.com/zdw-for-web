package com.zdw.zfw.admin.modular.system.controller;

import com.zdw.zfw.admin.core.constants.SysConstants;
import com.zdw.zfw.admin.modular.system.dao.SysRoleMapper;
import com.zdw.zfw.admin.modular.system.model.entity.SysRole;
import com.zdw.zfw.admin.modular.system.model.entity.SysRoleMenu;
import com.zdw.zfw.admin.modular.system.service.ISysRoleService;
import com.zdw.zfw.core.http.HttpResult;
import com.zdw.zfw.core.page.PageRequest;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 角色控制器
 * @author zdw
 * @date Oct 29, 2018
 */
@RestController
@RequestMapping("role")
@RequiresAuthentication
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SysRoleController {
	/**
	 * 角色 service
	 */
	private final ISysRoleService sysRoleService;

	/**
	 * 角色 mapper
	 */
	private final SysRoleMapper sysRoleMapper;
	
	@PostMapping(value="/save")
	public HttpResult save(@RequestBody SysRole record) {
		SysRole role = sysRoleService.findById(record.getId());
		if(role != null) {
			if(SysConstants.ADMIN.equalsIgnoreCase(role.getName())) {
				return HttpResult.error("超级管理员不允许修改!");
			}
		}
		boolean isExist = (record.getId() == null || record.getId() ==0) && !sysRoleService.findByName(record.getName()).isEmpty();
		// 新增角色
		if(isExist) {
			return HttpResult.error("角色名已存在!");
		}
		return HttpResult.success(sysRoleService.saveRecord(record));
	}

	@PostMapping(value="/delete")
	public HttpResult delete(@RequestBody List<SysRole> records) {
		return HttpResult.success(sysRoleService.delete(records));
	}

	@PostMapping(value="/findPage")
	public HttpResult findPage(@RequestBody PageRequest pageRequest) {
		return HttpResult.success(sysRoleService.findPage(pageRequest));
	}
	
	@GetMapping(value="/findAll")
	public HttpResult findAll() {
		return HttpResult.success(sysRoleService.findAll());
	}
	
	@GetMapping(value="/findRoleMenus")
	public HttpResult findRoleMenus(@RequestParam Long roleId) {
		return HttpResult.success(sysRoleService.findRoleMenus(roleId));
	}
	
	@PostMapping(value="/saveRoleMenus")
	public HttpResult saveRoleMenus(@RequestBody List<SysRoleMenu> records) {
		for(SysRoleMenu record:records) {
			SysRole sysRole = sysRoleMapper.selectByPrimaryKey(record.getRoleId());
			if(SysConstants.ADMIN.equalsIgnoreCase(sysRole.getName())) {
				// 如果是超级管理员，不允许修改
				return HttpResult.error("超级管理员拥有所有菜单权限，不允许修改！");
			}
		}
		return HttpResult.success(sysRoleService.saveRoleMenus(records));
	}
}
