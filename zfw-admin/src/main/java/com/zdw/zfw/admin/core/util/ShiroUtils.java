package com.zdw.zfw.admin.core.util;

import com.zdw.zfw.admin.modular.system.model.entity.LoginUser;
import org.apache.shiro.SecurityUtils;

/**
 * Shiro相关工具类
 *
 * @author zdw
 * @date Oct 29, 2018
 */
public class ShiroUtils {

	/**
	 * 获取当前登录用户
	 * @return 前登录用户
	 */
	public static LoginUser getUser() {
		Object user = SecurityUtils.getSubject().getPrincipal();
		return user == null ? null : ((LoginUser) user);
	}

	/**
	 * 判断是否登录
	 *
	 * @return 是否登录
	 */
	public static boolean isLogin() {
		return SecurityUtils.getSubject().getPrincipal() != null;
	}

	/**
	 * 登出
	 */
	public static void logout() {
		SecurityUtils.getSubject().logout();
	}
	
}
