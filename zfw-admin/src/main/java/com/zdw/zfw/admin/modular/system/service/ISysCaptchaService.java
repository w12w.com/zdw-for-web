package com.zdw.zfw.admin.modular.system.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 验证码的Service
 *
 * @author zdw
 */
public interface ISysCaptchaService {

    /**
     * 生成验证码
     *
     * @param request request
     * @param response request
     */
    void generateCaptcha(HttpServletResponse response, HttpServletRequest request) throws IOException;

    /**
     * 检查验证码
     *
     * @param cookie cookie
     * @param captcha 验证码
     * @return 是否通过验证
     */
     String checkCaptcha(String cookie, String captcha);
}
