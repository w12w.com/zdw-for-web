package com.zdw.zfw.admin.core.constants;

/**
 * 常量管理
 * @author zdw
 * @date Oct 29, 2018
 */ 
public interface SysConstants {

	/**
	 * 系统管理员用户名
	 */
	String ADMIN = "admin";

	/**
	 * token的请求头
	 */
	String TOKEN_HEADER = "access_token";
	
}
