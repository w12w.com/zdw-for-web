package com.zdw.zfw.admin.modular.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zdw.zfw.admin.modular.system.model.entity.SysLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysLogMapper extends BaseMapper<SysLog> {


    /**
     * @param id
     * @return
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新增一条记录
     *
     * @param record 记录
     * @return 新增数量
     */
    @Override
    int insert(SysLog record);

    /**
     * @param record
     * @return
     */
    int insertSelective(SysLog record);

    /**
     * @param id
     * @return
     */
    SysLog selectByPrimaryKey(Long id);

    /**
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(SysLog record);

    /**
     * @param record
     * @return
     */
    int updateByPrimaryKey(SysLog record);

    /**
     * @return
     */
    List<SysLog> findPage();

    /**
     *
     * @param userName 用户名
     * @return
     */
    List<SysLog> findPageByUserName(@Param(value = "userName") String userName);

}