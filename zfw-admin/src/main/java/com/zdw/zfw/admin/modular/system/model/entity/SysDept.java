package com.zdw.zfw.admin.modular.system.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zdw.zfw.core.base.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 部门实体
 * @author zdw
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
@TableName("sys_dept")
public final class SysDept extends BaseModel<SysDept> {

    private String name;

    @TableField(value = "parent_id")
    private Long parentId;

	@TableField(value = "order_num")
    private Integer orderNum;

	@TableField(value = "del_flag")
    private Byte delFlag;

	@TableField(exist = false)
    private List<SysDept> children;

	/**
	 * 非数据库字段
	 */
	@TableField(exist = false)
	private String parentName;

	/**
	 * 非数据库字段
	 */
	@TableField(exist = false)
    private Integer level;
}