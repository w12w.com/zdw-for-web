package com.zdw.zfw.admin.modular.system.model.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 登录用户信息的vo
 *
 * @author zdw
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
public class LoginUserVO {
    /**
     * 用户id
     */
    private Long id;

    /**
     *  用户名
     */
    private String name;

    /**
     * 用户邮箱
     */
    private String email;

    /**
     * 用户电话
     */
    private String mobile;

    /**
     * 部门名
     */
    private String deptName;

    /**
     * 角色名
     */
    private String roleNames;

    /**
     * 用户简介
     */
    private String introduction;
}
