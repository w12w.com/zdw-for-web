package com.zdw.zfw.admin.modular.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zdw.zfw.admin.modular.system.model.entity.SysRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysRoleMapper extends BaseMapper<SysRole> {
    int deleteByPrimaryKey(Long id);

    int insert(SysRole record);

    int insertSelective(SysRole record);

    SysRole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysRole record);

    int updateByPrimaryKey(SysRole record);
    
    List<SysRole> findPage();

	List<SysRole> findAll();
	
	List<SysRole> findPageByName(@Param(value = "name") String name);

	List<SysRole> findByName(@Param(value = "name") String name);
}