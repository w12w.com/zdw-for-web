package com.zdw.zfw.admin.modular.dynamic.base.pojo;

import com.baomidou.mybatisplus.extension.service.IService;
import lombok.Data;

@Data
public class ProxyMetaData {
    private String path;

    IService service;

    String tableName;

}
