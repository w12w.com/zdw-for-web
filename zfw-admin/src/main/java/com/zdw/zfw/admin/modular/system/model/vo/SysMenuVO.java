package com.zdw.zfw.admin.modular.system.model.vo;

import com.zdw.zfw.core.base.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 菜单VO
 * @author zdw
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
public class SysMenuVO extends BaseModel<SysMenuVO> {

    /**
     * 父菜单id
     */
    private Long parentId;

    /**
     * 菜单名
     */
    private String name;

    /**
     * 地址
     */
    private String url;

    /**
     * 权限
     */
    private String perms;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 图标
     */
    private String icon;

    /**
     * 是否隐藏
     */
    private boolean hideInMenu;

    /**
     * 排序
     */
    private Integer orderNum;

    /**
     *删除标记
     */
    private Byte delFlag;

    /**
     * 父菜单
     */
    private String parentName;

    /**
     *等级
     */
    private Integer level;

    /**
     * 子菜单
     */
    private List<SysMenuVO> children;

    /**
     * 禁用
     */
    private Boolean disabled;
}