package com.zdw.zfw.admin.modular.dynamic.base.controller;

import com.zdw.zfw.core.http.HttpResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 模板基类
 *
 * @author zdw
 */
public interface BaseController {

    /**
     * 查询列表
     *
     * @param map 参数
     * @return 查询结果包装
     */
    @ResponseBody
    HttpResult list(@RequestBody(required = false) Map<String, Object> map);

    /**
     * 查询单条数据
     *
     * @param key 参数
     * @return 查询结果包装
     */
    @ResponseBody
    HttpResult get(String key);

    /**
     * 删除
     *
     * @param key 参数
     * @return 查询结果包装
     */
    @ResponseBody
    HttpResult delete(String key);

    /**
     * 更新
     *
     * @param map 参数
     * @return 查询结果包装
     */
    @ResponseBody
    HttpResult update(@RequestBody(required = false) Map<String, Object> map);

    /**
     * 新增
     *
     * @param map 参数
     * @return 查询结果包装
     */
    @ResponseBody
    HttpResult add(@RequestBody(required = false) Map<String, Object> map);
}
