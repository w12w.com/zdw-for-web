package com.zdw.zfw.admin.modular.system.controller;

import com.zdw.zfw.admin.modular.system.model.entity.SysDict;
import com.zdw.zfw.admin.modular.system.service.ISysDictService;
import com.zdw.zfw.core.http.HttpResult;
import com.zdw.zfw.core.page.PageRequest;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 字典控制器
 * @author zdw
 * @date Oct 29, 2018
 */
@RestController
@RequestMapping("dict")
@RequiresAuthentication
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SysDictController {

	/**
	 * 字典 service
	 */
	private final ISysDictService sysDictService;

	@PostMapping(value="/save")
	public HttpResult save(@RequestBody SysDict record) {
		return HttpResult.success(sysDictService.saveRecord(record));
	}

	@PostMapping(value="/delete")
	public HttpResult delete(@RequestBody List<SysDict> records) {
		return HttpResult.success(sysDictService.delete(records));
	}

	@PostMapping(value="/findPage")
	public HttpResult findPage(@RequestBody PageRequest pageRequest) {
		return HttpResult.success(sysDictService.findPage(pageRequest));
	}
	
	
	@GetMapping(value="/findByLable")
	public HttpResult findByLable(@RequestParam String lable) {
		return HttpResult.success(sysDictService.findByLabel(lable));
	}
}
