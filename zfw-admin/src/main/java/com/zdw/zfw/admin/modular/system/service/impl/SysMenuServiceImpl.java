package com.zdw.zfw.admin.modular.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zdw.zfw.admin.core.constants.SysConstants;
import com.zdw.zfw.admin.modular.system.dao.SysMenuMapper;
import com.zdw.zfw.admin.modular.system.model.entity.SysMenu;
import com.zdw.zfw.admin.modular.system.model.vo.SysMenuVO;
import com.zdw.zfw.admin.modular.system.service.ISysMenuService;
import com.zdw.zfw.core.page.MybatisPageHelper;
import com.zdw.zfw.core.page.PageRequest;
import com.zdw.zfw.core.page.PageResult;
import lombok.RequiredArgsConstructor;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {

    final Mapper mapper;
	@Override
	public int saveRecord(SysMenu record) {
		if(record.getId() == null || record.getId() == 0) {
			return this.baseMapper.insertSelective(record);
		}
		if(record.getParentId() == null) {
			record.setParentId(0L);
		}
		return this.baseMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int delete(SysMenu record) {
		return this.baseMapper.deleteByPrimaryKey(record.getId());
	}

	@Override
	public int delete(List<SysMenu> records) {
		for(SysMenu record:records) {
			delete(record);
		}
		return 1;
	}

	@Override
	public SysMenu findById(Long id) {
		return this.baseMapper.selectByPrimaryKey(id);
	}

	@Override
	public PageResult findPage(PageRequest pageRequest) {
		return MybatisPageHelper.findPage(pageRequest, this.baseMapper);
	}
	
	@Override
	public List<SysMenuVO> findTree(String userName, int menuType) {
		// 顶级菜单
		List<SysMenuVO> topMenus = new ArrayList<>();
		List<SysMenuVO> menuVOS = new ArrayList<>();
		List<SysMenu> menus = findByUser(userName);
		for (SysMenu menu : menus) {
			// 将数据库实体转化为VO
			SysMenuVO sysMenuVO = new SysMenuVO();
			mapper.map(menu ,sysMenuVO);
			menuVOS.add(sysMenuVO);
			//	如果是公共菜单 则在前端树组件会变为不可选状态且默认选中
			if (sysMenuVO.getPerms() != null && sysMenuVO.getPerms().contains(":public")){
				sysMenuVO.setDisabled(true);
			}
			if (sysMenuVO.getParentId() == null || menu.getParentId() == 0) {
				sysMenuVO.setLevel(0);
				topMenus.add(sysMenuVO);
			}
		}
		topMenus.sort(Comparator.comparing(SysMenuVO::getOrderNum));
		findChildren(topMenus, menuVOS, menuType);
		return topMenus;
	}

	@Override
	public List<SysMenu> findByUser(String userName) {
		if(userName == null || "".equals(userName) || SysConstants.ADMIN.equalsIgnoreCase(userName)) {
			return this.baseMapper.findAll();
		}
		return this.baseMapper.findByUserName(userName);
	}

	private void findChildren(List<SysMenuVO> sysMenus, List<SysMenuVO> menus, int menuType) {
		for (SysMenuVO sysMenu : sysMenus) {
			List<SysMenuVO> children = new ArrayList<>();
			for (SysMenuVO menu : menus) {
				if(menuType == 1 && menu.getType() == 2) {
					// 如果是获取类型不需要按钮，且菜单类型是按钮的，直接过滤掉
					continue ;
				}
				if (sysMenu.getId() != null && sysMenu.getId().equals(menu.getParentId())) {
					menu.setParentName(sysMenu.getName())
							.setLevel(sysMenu.getLevel() + 1);
					children.add(menu);
				}
			}
			sysMenu.setChildren(children);
			children.sort(Comparator.comparing(SysMenuVO::getOrderNum));
			findChildren(children, menus, menuType);
		}
	}
	
}
