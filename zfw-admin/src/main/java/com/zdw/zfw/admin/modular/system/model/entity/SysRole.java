package com.zdw.zfw.admin.modular.system.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zdw.zfw.core.base.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * 角色实体
 * @author zdw
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_role")
public final class SysRole extends BaseModel<SysRole> {

    private String name;

    private String remark;

    private Byte delFlag;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Byte getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Byte delFlag) {
		this.delFlag = delFlag;
	}

}