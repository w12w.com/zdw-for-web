package com.zdw.zfw.monitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

/**
 * 启动器
 * @author zdw
 * @date Oct 29, 2018
 */
@EnableAdminServer
@SpringBootApplication
public class ZfwMonitorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZfwMonitorApplication.class, args);
	}
}