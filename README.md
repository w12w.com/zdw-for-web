### 项目介绍

- Zfw是基于Spring Boot、Spring Cloud、Vue、Element实现的Java快速开发平台。
- 目标是搭建出一套简洁易用的快速解决方案，可以帮助用户有效降低项目开发难度和成本。
- 个人博客提供本项目开发过程同步系列教程文章，手把手的教你如何开发同类系统。

### 功能计划
- ✔ 系统登录：系统用户登录，系统登录认证（JWT token方式）
- ✔ 用户管理：新建用户，修改用户，删除用户，查询用户
- ✔ 机构管理：新建机构，修改机构，删除机构，查询机构
- ✔ 角色管理：新建角色，修改角色，删除角色，查询角色
- ✔ 菜单管理：新建菜单，修改菜单，删除菜单，查询菜单
- ✔ 字典管理：新建字典，修改字典，删除字典，查询字典
- ✔ 系统日志：记录用户操作日志，查看系统执行日志记录
- ✔ 数据监控：定制Druid信息，提供简洁有效的SQL监控
- ✔ 聚合文档：定制在线文档，提供简洁美观的API文档
- ✔ 备份还原：系统备份还原，一键恢复系统初始化数据
- ✔ 主题切换：支持主题切换，自定主题颜色，一键换肤
- ✔ 单点登录：利用 JWT, 提供统一的单点登录功能
- ✘ 微服务化：集成Spring Cloud，实现服务治理功能
- ✘ 服务监控：结合微服务治理，实现服务的全方位监控
- ✘ 服务限流：结合微服务治理，实现有效的服务限流
- ✘ 系统登录：集成第三方登录功能（QQ、微信、微博）
- ...

### 软件架构

#### 后端架构

##### 开发环境

- IDE : eclipse 4.x
- JDK : JDK1.8.x
- Maven : Maven 3.5.x
- MySQL: MySQL 5.7.x

##### 技术选型

- 核心框架：Spring Boot 2.x
- 安全框架：Apache Shiro 1.4.x
- 视图框架：Spring MVC 5.x
- 持久层框架：MyBatis 3.x
- 定时器：Quartz 2.x
- 数据库连接池：Druid 1.x
- 日志管理：SLF4J、Log4j

##### 项目结构

- zfw-core： 核心代码模块，主要封装公共业务模块
- zfw-admin： 后台管理模块，包含用户、角色、菜单管理等
- zfw-backup： 系统数据备份备份模块，可选择独立部署
- zfw-monitor： 系统监控服务端，监控Spring Boot服务模块

#### 后端安装

1. 下载源码

    git clone https://gitee.com/w12w.com/zdw-for-web.git

2. 导入工程

    使用 Eclipse导入 Maven 项目，在此之前请确认已安装 JDK 和 Maven 工具。

3. 编译代码

    找到 zfw-monitor 工程的 pom.xml，执行 maven clean install 命令编译打包。
    
    找到 zfw-admin 工程的 pom.xml，执行 maven clean install 命令编译打包。

    找到 zfw-backup 工程的 pom.xml，执行 maven clean install 命令编译打包。

4. 导入数据库

    新建 zfw 数据库，导入 zfw-admin 工程 doc 下的 SQL 脚本，导入初始化数据库。
    修改 zfw-admin 下 application.yml 中的数据库连接和账号密码为自己的数据库配置。

5. 启动系统

    找到 zfw-monitor 工程下的 ZfwMonitorApplication, 执行 Java 程序，启动项目。

    找到 zfw-admin 工程下的 ZfwAdminApplication, 执行 Java 程序，启动项目。

    找到 zfw-backup 工程下的 ZfwBackupApplication.java, 执行 Java 程序，启动项目。
    
    注意：监控服务器 monitor 要先启动，其他无所谓。


### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)