package com.zdw.zfw.backup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动器
 * @author zdw
 * @date Oct 29, 2018
 */
@SpringBootApplication(scanBasePackages={"com.zdw.zfw"})
public class ZfwBackupApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZfwBackupApplication.class, args);
	}
}