package com.zdw.zfw.core.exception;

/**
 * 框架封装的自定义业务异常 此异常信息一般用于返回给前台用户 请务必详细填写msg
 * @author zdw
 * @date Aug 21, 2018
 */
public class ZfwException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
    private String msg;
    private int code = 500;
    
    public ZfwException(String msg) {
		super(msg);
		this.msg = msg;
	}
	
	public ZfwException(String msg, Throwable e) {
		super(msg, e);
		this.msg = msg;
	}
	
	public ZfwException(String msg, int code) {
		super(msg);
		this.msg = msg;
		this.code = code;
	}
	
	public ZfwException(String msg, int code, Throwable e) {
		super(msg, e);
		this.msg = msg;
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
	
}
