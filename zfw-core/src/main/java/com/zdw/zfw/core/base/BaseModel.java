package com.zdw.zfw.core.base;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 基础模型
 * @author zdw
 * @date Sep 13, 2018
 */
@Getter
@Setter
@Accessors(chain = true)
public class BaseModel<T extends Model> extends Model<T> {

	@TableId(type = IdType.AUTO)
	private Long id;

	@TableField(value = "create_by", fill = FieldFill.INSERT)
    private String createBy;

	@TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

	@TableField(value = "last_update_by", fill = FieldFill.UPDATE)
    private String lastUpdateBy;

	@TableField(value = "last_update_time", fill = FieldFill.UPDATE)
    private Date lastUpdateTime;
}
