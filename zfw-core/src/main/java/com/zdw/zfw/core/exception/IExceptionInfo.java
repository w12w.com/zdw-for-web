package com.zdw.zfw.core.exception;

/**
 * 抽象的异常类
 *
 * @author zdw
 */
public interface IExceptionInfo {
    /**
     * 获取异常的状态码
     */
    Integer getCode();

    /**
     * 获取异常的提示信息
     */
    String getMessage();
}
